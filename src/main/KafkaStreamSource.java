package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaStreamSource extends Thread {

	File input;

	KafkaStreamSource(File input) {
		this.input = input;
	}

	public void run() {

		Properties env = System.getProperties();
		Properties props = new Properties();
		
		props.put("zk.connect", env.getOrDefault("zk.connect", "smd1.cloudapp.net:2181/"));
		props.put("bootstrap.servers", env.getOrDefault("bootstrap.servers", "smd1.cloudapp.net:9092"));

		props.put("serializer.class", "kafka.serializer.StringEncoder");
		props.put("key.serializer", org.apache.kafka.common.serialization.StringSerializer.class);
		props.put("value.serializer", org.apache.kafka.common.serialization.StringSerializer.class);

		System.err.println(props);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);

		try {
			InputStream gin = new GZIPInputStream(new FileInputStream(input));
			BufferedReader br = new BufferedReader(new InputStreamReader(gin));

			String line;
			while ((line = br.readLine()) != null) {
				line += "\n";
				System.err.println(line);
				try {
					ProducerRecord<String, String> data = new ProducerRecord<>("debs", line.substring(0, 1), line);
					producer.send(data);
				} catch (Exception x) {
				}
				Thread.sleep(1000);
			}
			br.close();
		} catch (Exception x) {
			x.printStackTrace();
		}
		producer.close();
	};

	public static void main(String[] args) {
		
		System.err.println( Arrays.asList( args ));
		if (args.length != 1) {
			System.err.println("usage: java KafkaStreamSource filename.gz");
			System.exit(0);
		}
		new KafkaStreamSource(new File(args[0])).start();
	}
}
